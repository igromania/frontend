import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NbThemeModule, NbLayoutModule, NbSidebarModule, NbMenuModule} from '@nebular/theme';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthService} from './core/services/auth.service';
import {SocketIoModule} from 'ng-socket-io';
import {HttpConfigureInterceptor} from './core/HttpConfigureInterceptor';
import {SocketService} from './core/services/socket.service';
import {ContextMenuModule} from 'ngx-contextmenu';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        NbThemeModule.forRoot({name: 'default'}),
        NbLayoutModule,
        NbSidebarModule.forRoot(),
        NbMenuModule.forRoot(),
        HttpClientModule,
        SocketIoModule.forRoot({
            url: 'ws://localhost:8080',
            options: {
                transports: ['websocket'],
                upgrade: true,
                port: 8080
            }
        }),
        ContextMenuModule.forRoot()
    ],
    providers: [
        AuthService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpConfigureInterceptor,
            multi: true
        },
        SocketService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
