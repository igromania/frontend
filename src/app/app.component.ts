import {Component} from '@angular/core';
import {SocketService} from './core/services/socket.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'igromania-frontend';

    constructor(private socketService: SocketService) {
        this.socketService.connect();
    }
}
