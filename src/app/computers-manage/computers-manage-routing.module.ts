import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ComputersManageComponent} from './computers-manage/computers-manage.component';

const routes: Routes = [
    {
        path: '',
        component: ComputersManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ComputersManageRoutingModule {
}
