import {Injectable} from '@angular/core';
import {ComputerListItem, ComputerStatus} from '../models/computer-list-item';
import {Observable, of} from 'rxjs';
import {getRandomEnum} from '../../core/utils/get-random-enum';

@Injectable({
    providedIn: 'root'
})
export class ComputersService {

    constructor() {
    }

    public getComputersList(): Observable<ComputerListItem[]> {
        const computers: ComputerListItem[] = [];

        for (let i = 1; i <= 15; i++) {
            computers.push({
                name: `Компьютер ${i}`,
                status: getRandomEnum(ComputerStatus),
                ip: `192.168.1.${i}`
            });
        }

        return of(computers);
    }
}
