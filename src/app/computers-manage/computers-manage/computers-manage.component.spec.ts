import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComputersManageComponent } from './computers-manage.component';

describe('ComputersManageComponent', () => {
  let component: ComputersManageComponent;
  let fixture: ComponentFixture<ComputersManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComputersManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComputersManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
