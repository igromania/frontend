import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ComputersManageRoutingModule} from './computers-manage-routing.module';
import {ComputersManageComponent} from './computers-manage/computers-manage.component';
import {ComputersListComponent} from './components/computers-list/computers-list.component';
import {
    NbAccordionModule,
    NbActionsModule, NbButtonModule,
    NbCardModule,
    NbContextMenuModule,
    NbLayoutModule,
    NbSelectModule,
    NbTreeGridModule
} from '@nebular/theme';
import {ContextMenuModule} from 'ngx-contextmenu';

@NgModule({
    declarations: [ComputersManageComponent, ComputersListComponent],
    imports: [
        CommonModule,
        ComputersManageRoutingModule,
        NbCardModule,
        NbLayoutModule,
        NbSelectModule,
        NbActionsModule,
        NbTreeGridModule,
        NbAccordionModule,
        NbContextMenuModule,
        NbButtonModule,
        ContextMenuModule.forRoot()
    ]
})
export class ComputersManageModule {
}
