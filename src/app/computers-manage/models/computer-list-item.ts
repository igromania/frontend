export enum ComputerStatus {
    FREE,
    BUSY,
    OFFLINE
}

export class ComputerListItem {
    id?: number;
    name?: string;
    status?: ComputerStatus;
    ip?: string;
}
