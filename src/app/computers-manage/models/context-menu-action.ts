export class ContextMenuAction {
    label?: string;
    click?: () => void;
    visible = true;
    icon?: string;
    isDivider = false;
}
