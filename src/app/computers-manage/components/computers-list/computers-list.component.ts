import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {ComputerListItem, ComputerStatus} from '../../models/computer-list-item';
import {ComputersService} from '../../services/computers.service';
import {SocketService} from '../../../core/services/socket.service';
import {ContextMenuComponent} from 'ngx-contextmenu';
import {ContextMenuAction} from '../../models/context-menu-action';

@Component({
    selector: 'app-computers-list',
    templateUrl: './computers-list.component.html',
    styleUrls: ['./computers-list.component.scss']
})
export class ComputersListComponent implements OnInit {

    @ViewChild('freeComputerContextMenu') public freeComputerContextMenu: ContextMenuComponent;
    @ViewChild('busyComputerContextMenu') public busyComputerContextMenu: ContextMenuComponent;
    @ViewChild('offlineComputerContextMenu') public offlineComputerContextMenu: ContextMenuComponent;

    public computers: ComputerListItem[] = [];

    public contextMenuActions = {
        freeComputer: [
            {
                label: 'Оплатить',
                click: this.onPayContextBtnClick,
                icon: 'far fa-money-bill-alt'
            },
            {
                label: 'Открыть',
                icon: 'fas fa-lock-open'
            },
            {
                isDivider: true
            },
            {
                label: 'Настройки',
                icon: 'fas fa-cog'
            },
        ] as ContextMenuAction[],
        busyComputer: [
            {
                label: 'Закончить сеанс',
                icon: 'fas fa-stop-circle'
            },
            {
                label: 'Доплатить',
                icon: 'fas fa-plus'
            },
            {
                label: 'Оштрафовать',
                icon: 'fas fa-receipt'
            },
            {
                isDivider: true
            },
            {
                label: 'Настройки',
                icon: 'fas fa-cog'
            },
        ] as ContextMenuAction[]
    };

    constructor(private computersService: ComputersService,
                private socketService: SocketService) {
        this.socketService.computersList.subscribe(computers => {
            this.loadData();
        });
    }

    ngOnInit() {
        this.loadData();
    }

    @HostListener('document:contextmenu')
    disableRightClick(event) {
        return false;
    }

    private loadData() {
        this.computers = this.socketService.computersList.getValue();
    }

    private onPayContextBtnClick(computer: ComputerListItem) {
        console.log('pay', computer.name);
    }

    public getStatusLabel(status: ComputerStatus) {
        switch (status) {
            case ComputerStatus.BUSY: {
                return 'Занят';
            }

            case ComputerStatus.FREE: {
                return 'Свободен';
            }

            case ComputerStatus.OFFLINE: {
                return '';
            }
        }
    }

    public getContextMenuForComputerListItem(computer: ComputerListItem) {
        switch (computer.status) {
            case ComputerStatus.BUSY: {
                return this.busyComputerContextMenu;
            }

            case ComputerStatus.FREE: {
                return this.freeComputerContextMenu;
            }

            case ComputerStatus.OFFLINE: {
                return this.offlineComputerContextMenu;
            }
        }
    }

}
