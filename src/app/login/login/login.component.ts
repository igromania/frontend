import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../core/services/auth.service';
import {AuthDto} from '../../core/models/dto/auth.dto';
import {Router} from '@angular/router';
import {SocketService} from '../../core/services/socket.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public form: FormGroup;

    constructor(private fb: FormBuilder,
                private authService: AuthService,
                private router: Router,
                private socketService: SocketService) {
        this.form = fb.group({
            login: ['', Validators.required],
            password: ['', Validators.required],
        });
    }

    ngOnInit() {
    }

    public onFormSubmit() {
        if (this.form.invalid) {
            return;
        }

        const dto: AuthDto = this.form.value;

        this.authService.auth(dto).subscribe(result => {
            if (result.success) {
                this.socketService.connect();
                this.router.navigate(['/']);
            }
        });
    }

}
