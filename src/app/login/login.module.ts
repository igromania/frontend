import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {LoginRoutingModule} from './login-routing.module';
import {NbButtonModule, NbCardModule, NbInputModule, NbLayoutModule} from '@nebular/theme';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    declarations: [LoginComponent],
    imports: [
        CommonModule,
        LoginRoutingModule,
        NbLayoutModule,
        NbCardModule,
        NbInputModule,
        NbButtonModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class LoginModule {
}
