import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './components/home/home.component';
import {NbActionsModule, NbLayoutModule, NbMenuModule, NbSidebarModule, NbUserModule} from '@nebular/theme';

@NgModule({
    declarations: [HomeComponent],
    imports: [
        CommonModule,
        HomeRoutingModule,
        NbSidebarModule,
        NbLayoutModule,
        NbMenuModule,
        NbUserModule,
        NbActionsModule
    ]
})
export class HomeModule {
}
