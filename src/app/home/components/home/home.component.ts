import {Component, OnInit} from '@angular/core';
import {NbMenuItem} from '@nebular/theme';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    items: NbMenuItem[] = [
        {
            title: 'Статистика',
            link: '/',
            icon: 'fab fa-dashcube'
        },
        {
            title: 'Компьютеры',
            link: '/dashboard',
            icon: 'fa fa-network-wired'
        },
        {
            title: 'Отчет',
            link: '/',
            icon: 'fas fa-chart-pie'
        },
        {
            title: 'Настройки',
            link: '/',
            icon: 'fa fa-cogs'
        }
    ];

    constructor() {
    }

    ngOnInit() {
    }

}
