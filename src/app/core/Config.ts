export class Config {
    private static root = '/api/';

    public static API_URL = {
        USERS: {
            ROOT: Config.root + 'user',
            AUTH: Config.root + 'users/auth'
        }
    };
}
