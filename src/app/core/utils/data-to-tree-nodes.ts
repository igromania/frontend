export function dataToTreeNodes<T>(data: any[]): TreeNode<T>[] {
    const nodes: TreeNode<T>[] = [];
    data.map(item => {
        nodes.push({
            children: null,
            data: item,
            expanded: false
        });
    });

    return nodes;
}
