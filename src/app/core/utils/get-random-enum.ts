export const getRandomEnum = (e) => {
    const enumLength = Object.keys(e).length / 2;
    return Math.floor(Math.random() * enumLength);
}
