import {Injectable, Injector} from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';

import {Observable} from 'rxjs';
import {tap} from 'rxjs/internal/operators';
import * as _ from 'lodash';


@Injectable()
export class HttpConfigureInterceptor implements HttpInterceptor {

    constructor(private inj: Injector) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const options: any = {
            withCredentials: false
        };

        if (req.url.startsWith('/api/')) {
            options.withCredentials = true;
        }

        const request = req.clone(options);

        return next.handle(request).pipe(tap((common) => {

        }, err => {

        }));
    }
}
