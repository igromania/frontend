import {EventEmitter, Injectable} from '@angular/core';
import {Socket} from 'ng-socket-io';
import {Router} from '@angular/router';
import {ComputerListItem} from '../../computers-manage/models/computer-list-item';
import {BehaviorSubject} from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class SocketService {
    public computersList = new BehaviorSubject<ComputerListItem[]>([]);

    constructor(private socket: Socket,
                private router: Router) {
        this.socket.on('connect', () => {
            console.log('connected');
            this.socket.emit('identity', {});
        });

        this.socket.on('disconnect', () => console.log('socket.io disconnected'));
        this.socket.on('reconnect', () => console.warn('socket.io reconnected'));
        this.socket.on('computers-list', (data) => {
            console.log('computers', data);
            this.computersList.next(data);
        });
    }

    connect() {
        if (this.socket.ioSocket.connected) {
            this.disconnect();
        }

        this.socket.connect();
    }

    disconnect() {
        this.socket.disconnect();
    }
}
