import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthDto} from '../models/dto/auth.dto';
import {CommonResponseDto} from '../models/dto/common-response.dto';
import {Config} from '../Config';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient) {
    }

    public auth(dto: AuthDto) {
        return this.http.post<CommonResponseDto>(Config.API_URL.USERS.AUTH, dto);
    }
}
